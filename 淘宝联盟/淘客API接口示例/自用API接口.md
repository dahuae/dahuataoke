### GET固定地址

`http://taoapi.httpec.com/topapi.php?`int=1&

> int=1时使用默认appkey

------

```
`http://taoapi.httpec.com/topapi.php?int=1&method=taobao.wireless.share.tpwd.create&tpwd_param={"logo":"https://m.taobao.com/xxx.jpg","text":"文字","url":"http://m.taobao.com","user_id":"0"}`
```

生成优惠券链接

**PID：**`mm_17398164_183650129_52272100273`

**SESSION:**`70002100830678569c78105eb6d8c68d368996bee61bf39f6c743323c5e703f459734c482248874`

**ID：**`595574808024`

API文档地址：https://open.taobao.com/api.htm?docId=28625&docType=2&scopeId=12403

获取session http://vipapi.dahuariji.com/

#### 拼接参数

> session=70002100830678569c78105eb6d8c68d368996bee61bf39f6c743323c5e703f459734c482248874&item_id=595574808024&adzone_id=52272100273&site_id=183650129&method=taobao.tbk.privilege.get

#### 完整URL

> http://taoapi.httpec.com/topapi.php?int=1&session=70002100830678569c78105eb6d8c68d368996bee61bf39f6c743323c5e703f459734c482248874&item_id=595574808024&adzone_id=52272100273&site_id=183650129&method=taobao.tbk.privilege.get