<?php
// +-----------------------------------------+
// | 淘宝TOP平台API调用接口(工具专用版本)    | 
// +-----------------------------------------+
// | Author: aMi <37433187@qq.com>           |
// +-----------------------------------------+
// | 说明:本接口支持POST/GET方式提交数据,格式|    
// |可参照开发平台API测试工具.int=0默认用户提|
// |交appkey和secret,int=1+递增时为切换内设权|
// |限,请自行判断返回Json错误代"code":7时递增|
// +-----------------------------------------+
//示列:GET?int=0&key=用户自己写&secret=用户自己写&method=taobao.wireless.share.tpwd.create&tpwd_param={"logo":"https://m.taobao.com/xxx.jpg","text":"文字","url":"http://m.taobao.com","user_id":"0"}


header('Content-Type:text/html;charset=UTF-8');

$keys=array(
$_REQUEST['key'], //默认用户提交appkey
'key1', //设置自己N个appkey 
'key2');

$secrets=array(
$_REQUEST['secret'], //默认用户提交secret
'secret1',//设置自己N个secret
'secret2');

//以下为原生API代码区 
$int=$_REQUEST['int'];
$app_key = $keys[$int];
$appSecret =$secrets[$int] ;
function createSign($paramArr) {
    global $appSecret;
    $sign = $appSecret;
    ksort($paramArr);
    foreach ($paramArr as $key => $val) {
        if ($key != '' && $val != '') {
            $sign.= $key . $val;
        }
    }
    $sign.= $appSecret;
    $sign = strtoupper(md5($sign));
    return $sign;
}
function createStrParam($paramArr) {
    $strParam = '';
    foreach ($paramArr as $key => $val) {
        if ($key != '' && $val != '') {
            $strParam.= $key . '=' . urlencode($val) . '&';
        }
    }
    return $strParam;
}
//如需固定API 可在下方数组内加入. 如'method'=> 'taobao.tbk.spread.get'
$paramArr = array('app_key' => $app_key, 'v' => '2.0', 'sign_method' => 'md5', 'format' => 'json', 'timestamp' => date('Y-m-d H:i:s'));
$paramArr = $paramArr + $_REQUEST;
$sign = createSign($paramArr);
$strParam = createStrParam($paramArr);
$strParam.= 'sign=' . $sign;
$url = 'http://gw.api.taobao.com/router/rest?' . $strParam;
$result = file_get_contents($url);
echo ($result);
?>