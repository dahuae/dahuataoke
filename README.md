# 大华淘客

#### 更新记录

2021-09-09 修复 关键词商品查询接口（） 部分中文关键词查询失败

2020-07-02 更新淘宝客SDK和生成器

2020-06-15 更新易语言SDK，用核心支持库重写，无需彗星支持库

2020-04-27 增加京东联盟API（易语言版）的SDK

2020-03-02 取消通用发单源码，更新淘客HTTP接口文档

2019-10-14 更新了API代码生成器，更新了通用发单源码

2019-7-23 更新了淘宝客模块，封装了60余个API，可以直接调用。


#### 介绍
几大联盟常用的API模块，如淘宝联盟API，京东联盟API，拼多多联盟API

#### 目录说明

详见目录名和目录内的REDEME.MD介绍
[京东联盟API更新记录](https://gitee.com/dahuae/dahuataoke/blob/master/%E5%A4%A7%E5%8D%8E%E4%BA%AC%E4%B8%9CAPI/README.md)

#### 使用说明

1. 大华知道绝大部分同学都懒得用git，自行下载吧
2. 我知道我代码写的很烂，也别喷我了，一般你提的意见我也不会听
3. 多给大华一些黑科技吧


#### 小额赞助大华

![wx](https://img.alicdn.com/imgextra/i2/82248874/O1CN01AfFba72FQLHkwlsNE_!!82248874.jpg)（ ![alicode.jpg](https://img.alicdn.com/imgextra/i4/82248874/O1CN01pmLLi62FQLHkUzTrs_!!82248874.jpg) ）

**赞助名单**

云烟



   